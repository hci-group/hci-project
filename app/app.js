import React from 'react';
import { View } from 'react-native';
import { AppLoading, Font } from 'expo';
import { createStackNavigator } from 'react-navigation';
import * as Screens from './screens';
import { bootstrap } from './config/bootstrap';
import { data } from './data';

import Config from './config/config';
import Parse from 'parse/react-native';
import { AsyncStorage } from 'react-native';

bootstrap();
data.populateData();

const KittenApp = createStackNavigator(
  {
    First: Screens.SplashScreen,
    Login: Screens.Login,
    SignUp: Screens.SignUp,
    App: createStackNavigator(
      {
        // Walkthrough
        Walkthrough: Screens.WalkthroughScreen,
        // Main Map
        Map: Screens.Map,
        // Top Left
        Messages: Screens.Messages,
        Notifications: Screens.Notifications,
        Shop: Screens.Shop,
        ShopItem: Screens.ShopItem,
        // Bottom Left
        PetProfile: Screens.PetProfile,
        Dashboard: Screens.Dashboard,
        // Top Right
        Settings: Screens.Settings,
        // Bottom Right
        QRCode: Screens.QRCode,
        // Marker
        Marker: Screens.Marker,
        Owner: Screens.Owner,
        Member: Screens.Member,
        MemberList: Screens.MemberList,
        Chat: Screens.Chat,
        UserProfile: Screens.UserProfile,
      },
      {
        headerMode: 'screen',
      }
    ),
  },
  {
    headerMode: 'none',
  }
);

export default class App extends React.Component {
  state = {
    isLoaded: false,
  };

  componentWillMount() {
    this.loadAssets();

    Parse.setAsyncStorage(AsyncStorage);
    Parse.initialize(Config.appId, Config.javascriptKey);
    Parse.serverURL = Config.serverURL;
  }

  onNavigationStateChange = (previous, current) => {
    const screen = {
      current: this.getCurrentRouteName(current),
      previous: this.getCurrentRouteName(previous),
    };
  };

  getCurrentRouteName = (navigation) => {
    const route = navigation.routes[navigation.index];
    return route.routes ? this.getCurrentRouteName(route) : route.routeName;
  };

  loadAssets = async () => {
    await Font.loadAsync({
      fontawesome: require('./assets/fonts/fontawesome.ttf'),
      icomoon: require('./assets/fonts/icomoon.ttf'),
      'Righteous-Regular': require('./assets/fonts/Righteous-Regular.ttf'),
      'Roboto-Bold': require('./assets/fonts/Roboto-Bold.ttf'),
      'Roboto-Medium': require('./assets/fonts/Roboto-Medium.ttf'),
      'Roboto-Regular': require('./assets/fonts/Roboto-Regular.ttf'),
      'Roboto-Light': require('./assets/fonts/Roboto-Light.ttf'),
    });
    this.setState({ isLoaded: true });
  };

  renderLoading = () => (
    <AppLoading />
  );

  renderApp = () => (
    <View style={{ flex: 1 }}>
      <KittenApp onNavigationStateChange={this.onNavigationStateChange} />
    </View>
  );

  render = () => (this.state.isLoaded ? this.renderApp() : this.renderLoading());
}

Expo.registerRootComponent(App);
