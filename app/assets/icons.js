export const FontIcons = {
  login: '',
  navigation: '',
  article: '',
  profile: '',
  mail: '',
  dashboard: '',
  mobile: '',
  other: '',
  theme: '',
  card: '',
  addToCardForm: '',
};

export const FontAwesome = {
  heart: String.fromCharCode(61444),
  comment: String.fromCharCode(61669),
  user: String.fromCharCode(62144),
  twitter: String.fromCharCode(61593),
  google: String.fromCharCode(61856),
  facebook: String.fromCharCode(61594),
  plus: String.fromCharCode(61543),
  search: String.fromCharCode(61442),
  smile: String.fromCharCode(61720),
  chevronRight: String.fromCharCode(61524),
  chevronLeft: String.fromCharCode(61700),
  bars: String.fromCharCode(61641),
  slashEye: String.fromCharCode(61552),
  github: String.fromCharCode(61595),

  message: String.fromCharCode(61664),
  notification: String.fromCharCode(61738),
  shop: String.fromCharCode(61781),
  settings: String.fromCharCode(61459),
  profile: String.fromCharCode(61872),
  gym: String.fromCharCode(63198),
  food: String.fromCharCode(62183),
  park: String.fromCharCode(61883),


  added: String.fromCharCode(61452),
  alert: String.fromCharCode(61738),  
  level: String.fromCharCode(61683),
  friends: String.fromCharCode(62720),

  qrcode: String.fromCharCode(61481),

};
