import React from 'react';
import {
  Image,
  View,
} from 'react-native';
import {
  RkComponent,
  RkText,
  RkTheme,
} from 'react-native-ui-kitten';
import { FontAwesome } from '../../assets/icons';

export class Avatar extends RkComponent {
  componentName = 'Avatar';
  typeMapping = {
    container: {},
    image: {},
    badge: {},
    badgeText: {},
  };

  getBadgeStyle = (badgeProps) => {
    switch (badgeProps) {
      case 'like':
        return {
          symbol: FontAwesome.heart,
          backgroundColor: RkTheme.current.colors.badge.likeBackground,
          color: RkTheme.current.colors.badge.likeForeground,
        };
      case 'follow':
        return {
          symbol: FontAwesome.plus,
          backgroundColor: RkTheme.current.colors.badge.plusBackground,
          color: RkTheme.current.colors.badge.plusForeground,
        };
      case 'added':
        return {
          symbol: FontAwesome.added,
          backgroundColor: RkTheme.current.colors.badge.addBackground,
          color: RkTheme.current.colors.badge.addForeground,
        };

      case 'alert':
        return {
          symbol: FontAwesome.alert,
          backgroundColor: RkTheme.current.colors.badge.alertBackground,
          color: RkTheme.current.colors.badge.alertForeground,
        };  

      case 'level':
        return {
          symbol: FontAwesome.level,
          backgroundColor: RkTheme.current.colors.badge.levelBackground,
          color: RkTheme.current.colors.badge.levelForeground,
        };  
      case 'friends':
        return {
          symbol: FontAwesome.friends,
          backgroundColor: RkTheme.current.colors.badge.friendBackground,
          color: RkTheme.current.colors.badge.friendForeground,
        };  
      case 'smile':
        return {
          symbol: FontAwesome.smile,
          backgroundColor: RkTheme.current.colors.badge.smileBackground,
          color: RkTheme.current.colors.badge.smileForeground,
        };  
      default: return {};
    }
  };

  renderImg = (styles) => (
    <View>
      <Image style={styles.image} source={this.props.img} />
      {this.props.badge && this.renderBadge(styles.badge)}
    </View>
  );

  renderBadge = (style, textStyle) => {
    const badgeStyle = this.getBadgeStyle(this.props.badge);
    return (
      <View style={[style, { backgroundColor: badgeStyle.backgroundColor }]}>
        <RkText rkType='awesome' style={[textStyle, { color: badgeStyle.color }]}>
          {badgeStyle.symbol}
        </RkText>
      </View>
    );
  };

  render() {
    const { container, ...other } = this.defineStyles();
    return (
      <View style={[container, this.props.style]}>
        {this.renderImg(other)}
      </View>
    );
  }
}
