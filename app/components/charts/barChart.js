import React from 'react';
import {
  View,
  Dimensions,
} from 'react-native';
import {
  RkComponent,
  RkTheme,
  RkText,
} from 'react-native-ui-kitten';

import {
  VictoryChart,
  VictoryAxis,
  VictoryBar,
  VictoryLine,
  VictoryLabel
} from 'victory-native';

export class BarChart extends RkComponent {

  componentWillMount() {
    const data = this.props.data;

    let average = 100;
    let goal = 120;

    this.size = Dimensions.get('window').width - 60;

    let xTicks = data.map((({ x }) => x));

    let yValues = data.map((({ y }) => y));
    let maxY = Math.max(...yValues);

    let averageData = data.map(({ x }) => ({ x, y: average }));
    let averageLabel = { x: 45, y: this.size + 20 - (this.size * average / maxY) };
    let goalData = data.map(({ x }) => ({ x, y: goal }));
    let goalLabel = { x: 45, y: this.size + 20 - (this.size * goal / maxY) };

    this.setState({ data, xTicks, averageData, averageLabel, goalData, goalLabel });
  }

  render = () => (
    <View>
      <RkText rkType='header4'>{this.props.title}</RkText>
      <VictoryChart
        padding={{
          top: 20, left: 40, right: 5, bottom: 15,
        }}
        height={this.size}
        width={this.size}
      >

        <VictoryAxis
          tickValues={this.state.xTicks}
          tickLabelComponent={<VictoryLabel dy={5} />}
          style={{
            axis: { stroke: RkTheme.current.colors.disabled, strokeWidth: 0.5 },
            tickLabels: {
              fontSize: 14,
              stroke: RkTheme.current.colors.text.secondary,
              fill: RkTheme.current.colors.text.secondary,
              fontFamily: RkTheme.current.fonts.family.regular,
              strokeWidth: 0.5,
            },
          }}
        />
        <VictoryAxis
          dependentAxis
          tickLabelComponent={<VictoryLabel dy={15} />}
          style={{
            axis: { stroke: RkTheme.current.colors.disabled, strokeWidth: 0.5 },
            grid: { stroke: RkTheme.current.colors.disabled, strokeWidth: 0.5 },
            tickLabels: {
              fontSize: 14,
              stroke: RkTheme.current.colors.text.secondary,
              fill: RkTheme.current.colors.text.secondary,
              fontFamily: RkTheme.current.fonts.family.regular,
              strokeWidth: 0.5,
            },
          }}
        />

        <VictoryLine
          style={{
            data: {
              fill: RkTheme.current.colors.charts.area.fill,
              fillOpacity: 0.5,
              stroke: RkTheme.current.colors.charts.area.stroke,
              strokeOpacity: 0.8,
              strokeWidth: 1.5,
            },
          }}
          data={this.state.averageData}
        />

        <VictoryLabel
          x={this.state.averageLabel.x}
          y={this.state.averageLabel.y}
          verticalAnchor='end'
          textAnchor='start'
          text='average'
          style={{
            data: {
              fill: RkTheme.current.colors.charts.area.fill,
              fillOpacity: 0.5,
              stroke: RkTheme.current.colors.charts.area.stroke,
              strokeOpacity: 0.8,
              strokeWidth: 1.5,
            },
          }}
        />

        <VictoryLine
          style={{
            data: {
              fill: RkTheme.current.colors.charts.area.fill,
              fillOpacity: 0.5,
              stroke: RkTheme.current.colors.charts.area.stroke,
              strokeOpacity: 0.8,
              strokeWidth: 1.5,
            },
          }}
          data={this.state.goalData}
        />

        <VictoryLabel
          x={this.state.goalLabel.x}
          y={this.state.goalLabel.y}
          verticalAnchor='end'
          textAnchor='start'
          text='goal'
          style={{
            data: {
              fill: RkTheme.current.colors.charts.area.fill,
              fillOpacity: 0.5,
              stroke: RkTheme.current.colors.charts.area.stroke,
              strokeOpacity: 0.8,
              strokeWidth: 1.5,
            },
          }}
        />

        <VictoryBar
          data={this.state.data}
          style={{
            data: {
              fill: RkTheme.current.colors.charts.area.fill,
              fillOpacity: 0.5,
              stroke: RkTheme.current.colors.charts.area.stroke,
              strokeOpacity: 0.8,
              strokeWidth: 1.5,
            },
          }}
        />
      </VictoryChart>
    </View>
  );
}
