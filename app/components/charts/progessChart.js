import React from 'react';
import { View } from 'react-native';
import {
  RkComponent,
  RkText,
  RkTheme,
  RkStyleSheet,
} from 'react-native-ui-kitten';
import { VictoryPie } from 'victory-native';
import { Svg, Text as SvgText } from 'react-native-svg';
import { scale } from '../../utils/scale';

export class ProgressChart extends RkComponent {
  state = {
    percents: 55.0,
    currentSteps: 554,
    goalSteps: 1000,
  };
  size = 100;
  fontSize = 20;

  componentDidMount() {
    this.setStateInterval = setInterval(this.updatePercent, 2000);
  }

  componentWillUnmount() {
    clearInterval(this.setStateInterval);
  }

  sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
  }  

  updatePercent = async () => {
    await this.sleep(this.getRandomInt(500));
    let currentSteps = this.state.currentSteps;
    currentSteps += 1;
    let percents = parseFloat((currentSteps / this.state.goalSteps * 100).toFixed(1))
    this.setState({ percents, currentSteps });
  };

  getChartData = () => [
    { x: 1, y: this.state.percents },
    { x: 2, y: 100 - this.state.percents },
  ];

  onChartFill = (data) => {
    const themeColor = RkTheme.current.colors.charts.followersProgress;
    return data.x === 1 ? themeColor : 'transparent';
  };

  render = () => (
    <View>
      
      <View style={styles.chartContainer}>
        <Svg width={scale(this.size)} height={scale(this.size)}>
          <VictoryPie
            labels={[]}
            padding={0}
            standalone={false}
            width={scale(this.size)}
            height={scale(this.size)}
            style={{ data: { fill: this.onChartFill } }}
            data={this.getChartData()}
            cornerRadius={scale(25)}
            innerRadius={scale(40)}
          />
          
          <SvgText
            textAnchor="middle"
            verticalAnchor="middle"
            x={scale(this.size / 2)}
            y={scale((this.size /2)) + 8}
            height={scale(this.size / 3)}
            fontSize={scale(this.fontSize)}
            fontFamily={RkTheme.current.fonts.family.regular}
            stroke={RkTheme.current.colors.text.base}
            fill={RkTheme.current.colors.text.base}>
            {`${this.state.percents}%`}
          </SvgText>
        </Svg>
        <View>
          <RkText rkType='header4'>REACH</RkText>
          <RkText rkType='header2'>{this.state.currentSteps}/{this.state.goalSteps}</RkText>
          <RkText rkType='secondary2'>+6 per day in average</RkText>
        </View>
      </View>
    </View>
  );
}

const styles = RkStyleSheet.create(() => ({
  chartContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 10,
  },
}));
