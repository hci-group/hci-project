import _ from 'lodash';
import populate from './dataGenerator';
import users from './raw/users';
import articles from './raw/articles';
import notifications from './raw/notifications';
import conversations from './raw/conversations';

class DataProvider {
  getUser(id = 1) {
    return _.find(users, x => x.id === id);
  }

  getNotifications() {
    return notifications;
  }

  getArticles(type = 'article') {
    return _.filter(articles, x => x.type === type);
  }

  getArticle(id) {
    return _.find(articles, x => x.id === id);
  }

  getChatList() {
    return conversations;
  }

  populateData() {
    populate();
  }

  getPhoto(id) {
    const images = [
      require('./img/Image10.png'),
      require('./img/Image11.png'),
      require('./img/Image2.png'),
      require('./img/Image3.png'),
      require('./img/Image4.png'),
      require('./img/Image1.png'),
      require('./img/Image12.png'),
      require('./img/Image8.png'),
      require('./img/Image6.png'),
      require('./img/Image9.png'),
      require('./img/Image5.png'),
      require('./img/Image7.png'),
      require('./img/Image13.png'),
      require('./img/Image14.png'),
      require('./img/pet1.png'),
      require('./img/DARK.png'),
    ];
    return images[id];
  }
}

export const data = new DataProvider();
