export const Conversations = [
  {
    withUserId: 1,
    messages: [
      {
        id: 0,
        type: 'out',
        time: -300,
        text: 'Hey Zhao Rong, I will be hitting the gym at Tampines today.',
      },
      {
        id: 1,
        time: -240,
        type: 'out',
        text: 'Oh nice! I am heading there to hatch some eggs.',
      },
      {
        id: 2,
        time: -100,
        type: 'in',
        text: 'Lets go together!',
      },
      {
        id: 3,
        time: -45,
        type: 'in',
        text: 'Then can we go and get healthy eateries? lol',
      },
      {
        id: 4,
        time: -25,
        type: 'out',
        text: 'Yes we can, let\'s go! ',
      },
      ],
  },

  {
    withUserId: 2,
    messages: [
      {
        id: 0,
        type: 'in',
        time: -300,
        text: 'I need to go hatch egg, lets hit the gym',
      },
      {
        id: 1,
        time: -240,
        type: 'out',
        text: 'Oh nice! I am heading there now.',
      },

      {
        id: 2,
        time: -140,
        type: 'out',
        text: 'Thank you, next!',
      },
      ],
  },

  {
    withUserId: 3,
    messages: [
      {
        id: 0,
        type: 'in',
        time: -300,
        text: 'asdgaisdgiahdihasdasdd',
      },
      {
        id: 1,
        time: -240,
        type: 'out',
        text: 'asdsadasoiduadosaodoajdsasdas',
      },

      {
        id: 2,
        time: -140,
        type: 'out',
        text: 'HAHA!',
      },
      ],
  },
];

export default Conversations;
