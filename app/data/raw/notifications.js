const notifications = [
  {
    id: 1,
    type: 'added',
    description: 'just joined group, time to play!',
    time: -1,
  }, {
    id: 2,
    type: 'level',
    description: 'has gotten a legendary egg!',
    time: -271,
    attach: require('../img/Image8.png'),
  }, {
    id: 3,
    type: 'alert',
    description: 'has invited you into a group!',
    time: -541,
    attach: require('../img/Image8.png'),
  }, {
    id: 4,
    type: 'level',
    description: 'has gotten a rare egg!',
    time: -811,
  }, {
    id: 5,
    type: 'alert',
    description: 'has invited you into a group!',
    time: -1081,
  }, {
    id: 6,
    type: 'alert',
    description: 'has invited you into a group!',
    time: -1351,
  }, {
    id: 7,
    type: 'added',
    description: 'requested you to join group, time to play!',
    time: -1621,
    attach: require('../img/Image8.png'),
  }, {
    id: 8,
    type: 'smile',
    description: 'has joined the group, Hunters!',
    time: -1891,
  }, {
    id: 9,
    type: 'smile',
    description: 'has joined the group, Hunters!',
    time: -2161,
  }, {
    id: 10,
    type: 'level',
    description: 'pet has just evolve to rare!',
    time: -2431,
    attach: require('../img/Image8.png'),
  },
];

export default notifications;
