import React from 'react';
import {
  Alert,
  View,
  Image,
  Dimensions,
  Keyboard,
  Vibration,
} from 'react-native';
import {
  RkButton,
  RkText,
  RkTextInput,
  RkAvoidKeyboard,
  RkStyleSheet,
  RkTheme,
} from 'react-native-ui-kitten';
import { GradientButton } from '../../components/gradientButton';
import { scaleModerate, scaleVertical } from '../../utils/scale';
import NavigationType from '../../config/navigation/propTypes';

import { Notifications } from 'expo';

import Parse from 'parse/react-native';

@observer
export class Login extends React.Component {
  static propTypes = {
    navigation: NavigationType.isRequired,
  };
  static navigationOptions = {
    header: null,
  };

  getThemeImageSource = (theme) => (
    theme.name === 'light' ?
      require('../../assets/images/login.png') : require('../../assets/images/login.png')
  );

  renderImage = () => {
    const screenSize = Dimensions.get('window');
    const imageSize = {
      width: screenSize.width,
      height: screenSize.height - scaleModerate(375, 1),
    };
    return (
      <Image
        style={[styles.image, imageSize]}
        source={this.getThemeImageSource(RkTheme.current)}
      />
    );
  };

  onLoginButtonPressed = () => {
    Parse.User.logIn(this.state.username, this.state.password).then((user) => {
      let channel = { name: 'Wolfzara', sound: true, vibrate: true };
      Notifications.createChannelAndroidAsync('1', channel);
      let localNotification = { title: 'Wolfzara', body: 'Master! I am hungry please feed me...', channelId: '1' };
      let schedulingOptions = { time: new Date().getTime() + 60000 };
      Notifications.scheduleLocalNotificationAsync(localNotification, schedulingOptions);
      this.props.navigation.navigate('Walkthrough');
    }).catch((error)=> {
      Alert.alert('Login failed!', 'Please check your login credentials again!')
    })
  };

  onTextChange = (name, value) => {
    this.setState({ [name]: value });
  }

  onSignUpButtonPressed = () => {
    this.props.navigation.navigate('SignUp');
  };

  render = () => (
    <RkAvoidKeyboard
      onStartShouldSetResponder={() => true}
      onResponderRelease={() => Keyboard.dismiss()}
      style={styles.screen}>
      {this.renderImage()}
      <View style={styles.container}>
        <View style={styles.buttons}>
          <Image
            source={require('../../assets/images/login1.png')}
          />
        </View>
        <RkTextInput rkType='rounded' placeholder='Username' onChangeText={value => this.onTextChange('username', value)} />
        <RkTextInput rkType='rounded' placeholder='Password' onChangeText={value => this.onTextChange('password', value)} secureTextEntry />
        <GradientButton
          style={styles.save}
          rkType='large'
          onPress={this.onLoginButtonPressed}
          text='LOGIN'
        />
        <View style={styles.footer}>
          <View style={styles.textRow}>
            <RkText rkType='primary3'>Don’t have an account? </RkText>
            <RkButton rkType='clear'>
              <RkText rkType='header6' onPress={this.onSignUpButtonPressed}>Sign up now</RkText>
            </RkButton>
          </View>
        </View>
      </View>
    </RkAvoidKeyboard>
  )
}

const styles = RkStyleSheet.create(theme => ({
  screen: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.colors.screen.base,
  },
  image: {
    resizeMode: 'cover',
    marginBottom: scaleVertical(10),
  },
  container: {
    paddingHorizontal: 17,
    paddingBottom: scaleVertical(22),
    alignItems: 'center',
    flex: -1,
  },
  footer: {
    justifyContent: 'flex-end',
    flex: 1,
  },
  buttons: {
    flexDirection: 'row',
    marginBottom: scaleVertical(24),
  },
  button: {
    marginHorizontal: 14,
  },
  save: {
    marginVertical: 9,
  },
  textRow: {
    justifyContent: 'center',
    flexDirection: 'row',
  },
}));
