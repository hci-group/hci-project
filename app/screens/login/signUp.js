import React from 'react';
import {
  View,
  Image,
  Alert,
  Vibration,
  Keyboard,
} from 'react-native';
import {
  RkButton,
  RkText,
  RkTextInput,
  RkStyleSheet,
  RkTheme,
  RkAvoidKeyboard,
} from 'react-native-ui-kitten';
import { GradientButton } from '../../components/';
import { scaleVertical } from '../../utils/scale';
import NavigationType from '../../config/navigation/propTypes';
import Parse from 'parse/react-native';

export class SignUp extends React.Component {
  static navigationOptions = {
    header: null,
  };
  static propTypes = {
    navigation: NavigationType.isRequired,
  };

  state = {
    users: []
  }

  onNameChange = (name, value) => {
    this.setState({ [name]: value });
  }

  onCreateButtonPressed = () => {
    var user = new Parse.User();
    user.set("username", this.state.username);
    user.set("password", this.state.password);
    user.signUp();
    
    this.props.navigation.goBack();
    Alert.alert(
      'Account Created',
      'You are now able to login',
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false }
    )
    PATTERN = [500, 600];
    Vibration.vibrate(PATTERN, false);
  };

  render = () => (
    <RkAvoidKeyboard
      style={styles.screen}
      onStartShouldSetResponder={() => true}
      onResponderRelease={() => Keyboard.dismiss()}>
      <View style={{ alignItems: 'center' }}>
        <RkText rkType='h1'>Registration</RkText>
      </View>
      <View style={styles.container}>
          <Image
            source={require('../../assets/images/register.png')}
          />
        </View>
      <View style={styles.content}>
        <View>
          <RkTextInput rkType='rounded' placeholder='Username' onChangeText={(value) => this.onNameChange('username', value)} />
          <RkTextInput rkType='rounded' placeholder='Password' onChangeText={(value) => this.onNameChange('password', value)} secureTextEntry/>
          <GradientButton
            style={styles.save}
            rkType='large'
            text='CREATE USER'
            onPress={this.onCreateButtonPressed}
          />
        </View>
        <View style={styles.footer}>
          <View style={styles.textRow}>
            <RkText rkType='primary3' />
          </View>
        </View>
      </View>
    </RkAvoidKeyboard>
  )
}

const styles = RkStyleSheet.create(theme => ({
  screen: {
    padding: 16,
    flex: 1,
    justifyContent: 'space-around',
    backgroundColor: theme.colors.screen.base,
  },
  image: {
    marginBottom: 10,
    height: scaleVertical(77),
    resizeMode: 'contain',
  },
  content: {
    justifyContent: 'space-between',
  },
  save: {
    marginVertical: 20,
  },
  buttons: {
    flexDirection: 'row',
    marginBottom: 24,
    marginHorizontal: 24,
    justifyContent: 'space-around',
  },
  container: {
    paddingHorizontal: 17,
    paddingBottom: scaleVertical(22),
    alignItems: 'center',
    flex: -1,
  },
  footer: {
    justifyContent: 'flex-end',
  },
  textRow: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
}));
