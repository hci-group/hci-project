import React from 'react';
import { View, ScrollView } from 'react-native';
import { RkText, RkStyleSheet, RkTheme } from 'react-native-ui-kitten';
import { FontAwesome } from '../../assets/icons';
import { BarChart } from '../../components/';

const gymData = [
  { x: 1, y: 60 },
  { x: 2, y: 90 },
  { x: 3, y: 50 },
  { x: 4, y: 90 },
  { x: 5, y: 120 },
  { x: 6, y: 120 },
  { x: 7, y: 150 },
];

const foodData = [
  { x: 1, y: 10 },
  { x: 2, y: 80 },
  { x: 3, y: 40 },
  { x: 4, y: 90 },
  { x: 5, y: 120 },
  { x: 6, y: 120 },
  { x: 7, y: 150 },
];

const parkData = [
  { x: 1, y: 50 },
  { x: 2, y: 70 },
  { x: 3, y: 50 },
  { x: 4, y: 90 },
  { x: 5, y: 120 },
  { x: 6, y: 120 },
  { x: 7, y: 150 },

];

export class Dashboard extends React.Component {
  static navigationOptions = {
    title: 'Activities this week'.toUpperCase(),
  };

  state = {
    data: {
      statItems: [
        {
          name: 'Stars',
          value: '4,512',
          icon: 'github',
          background: RkTheme.current.colors.dashboard.stars,
        },
        {
          name: 'Tweets',
          value: '2,256',
          icon: 'twitter',
          background: RkTheme.current.colors.dashboard.tweets,
        },
        {
          name: 'Likes',
          value: '1,124',
          icon: 'facebook',
          background: RkTheme.current.colors.dashboard.likes,
        },
      ],
    },
  };

  renderStatItem = (item) => (
    <View style={[styles.statItemContainer, { backgroundColor: item.background }]} key={item.name}>
      <View>
        <RkText rkType='header6' style={styles.statItemValue}>{item.value}</RkText>
        <RkText rkType='secondary7' style={styles.statItemName}>{item.name}</RkText>
      </View>
      <RkText rkType='awesome hero' style={styles.statItemIcon}>{FontAwesome[item.icon]}</RkText>
    </View>
  );

  render = () => {
    const chartBackgroundStyle = { backgroundColor: RkTheme.current.colors.control.background };
    return (
      <ScrollView style={styles.screen}>
        <View style={[styles.chartBlock, chartBackgroundStyle]}>
          <BarChart title='Gym' data={gymData}/>
        </View>
        <View style={[styles.chartBlock, chartBackgroundStyle]}>
          <BarChart title='Healthy Eateries' data={foodData}/>
        </View>
        <View style={[styles.chartBlock, chartBackgroundStyle]}>
          <BarChart title='Park' data={parkData}/>
        </View>
      </ScrollView>
    );
  };
}

const styles = RkStyleSheet.create(theme => ({
  screen: {
    backgroundColor: theme.colors.screen.scroll,
    paddingHorizontal: 15,
  },
  chartBlock: {
    padding: 15,
    marginTop: 15,
    marginBottom: 15,
    justifyContent: 'center',
  },
}));

