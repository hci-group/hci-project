import React from 'react';
import { View, Alert } from 'react-native';
import NavigationType from '../../config/navigation/propTypes';

import MapView, { Marker } from 'react-native-maps';
import { Location, Permissions } from 'expo';

import { StyleSheet } from 'react-native';

import gymJson from '../../assets/geo/gym';
import foodJson from '../../assets/geo/food';
import parkJson from '../../assets/geo/park';

import { FontAwesome } from '../../assets/icons';

import { RkText, RkButton } from 'react-native-ui-kitten';
import Parse from 'parse/react-native';
import Config from '../../config/config';

export class Map extends React.Component {
  static navigationOptions = {
    header: null,
  };

  static propTypes = {
    navigation: NavigationType.isRequired,
  };

  state = {
    gyms: [],
    data: [],
    region: {
      latitude: 1.3521,
      longitude: 103.8198,
      latitudeDelta: 0.005,
      longitudeDelta: 0.005,
    },
  };

  getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      Alert.alert('Error', 'Please allow location', [{ text: 'OK' }], { cancelable: false });
      return;
    }

    let location = await Location.getCurrentPositionAsync({});

    this.onLocationChange(location);

    Location.watchPositionAsync({}, this.onLocationChange);
  };

  onLocationChange = (location) => {
    let region = {
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
      latitudeDelta: 0.005,
      longitudeDelta: 0.005,
    }

    var currentUser = Parse.User.current();
    currentUser.set('latitude', location.coords.latitude);
    currentUser.set('longtitude', location.coords.longitude);
    currentUser.save();

    this.setState({ region });
  }

  componentWillMount = () => {
    let gyms = this.processGeoJson(gymJson);
    let foods = this.processGeoJson(foodJson);
    let parks = this.processGeoJson(parkJson);
    this.setState({ gyms, foods, parks });

    this.getLocationAsync();

    var client = new Parse.LiveQueryClient({
      applicationId: Config.appId,
      serverURL: Config.wsServerURL,
      javascriptKey: Config.javascriptKey,
      masterKey: Config.masterKey
    });
    client.open();

    const User = Parse.Object.extend("User");
    const query = new Parse.Query(User);
    query.notEqualTo("objectId", Parse.User.current()._getId());

    const subscription = client.subscribe(query);

    subscription.on('update', (object) => {
      this.regetData(query);
    });

    this.regetData(query);
  };

  regetData = (query) => {
    query.find().then((users) => {
      var data = users.map((user) => {
        return {
          name: user.get('username'),
          coordinates: {
            latitude: user.get('latitude'),
            longitude: user.get('longtitude')
          }
        }
      })
      this.setState({ data });
    });
  }

  processGeoJson = (json) => {
    return json.features.map((feature) => ({
      name: feature.properties.Name,
      coordinates: {
        latitude: feature.geometry.coordinates[1],
        longitude: feature.geometry.coordinates[0]
      }
    }))
  }

  onMarkerPress = (place_name) => {
    this.props.navigation.navigate('Marker', { place_name });
  }

  onUserPress = (user_name) => {
    this.props.navigation.navigate('UserProfile', { username: user_name });
  }

  getUserMarker = (data) => (
    <Marker
      onPress={() => this.onUserPress(data.name)}
      key={data.name}
      title={data.name}
      coordinate={data.coordinates}
    >
      <RkButton rkType='placeicon circle' style={{ backgroundColor: 'black' }}>
        <RkText rkType='awesome place'>{String.fromCharCode(61447)}</RkText>
      </RkButton>
    </Marker>
  )

  getMarker = (place, key, icon, color) => (
    <Marker
      onPress={() => this.onMarkerPress(place.name)}
      key={key}
      title={place.name}
      coordinate={place.coordinates}
    >
      <RkButton rkType='placeicon circle' style={{ backgroundColor: color }}>
        <RkText rkType='awesome place'>{FontAwesome[icon]}</RkText>
      </RkButton>
    </Marker>
  )

  onMenuPress = (name) => {
    if (name === 'qrcode') {
      this.props.navigation.navigate('QRCode');
    }
    else if (name === 'shop') {
      this.props.navigation.navigate('Shop');
    }
    else if (name === 'notifications') {
      this.props.navigation.navigate('Notifications');
    }
    else if (name === 'profile') {
      this.props.navigation.navigate('PetProfile');
    }
    else if (name === 'messages') {
      this.props.navigation.navigate('Messages');
    }
    else if (name === 'settings') {
      this.props.navigation.navigate('Settings');
    }
  }

  render = () => (
    <View style={styles.map}>
      <MapView
        style={styles.map}
        region={this.state.region}
        showsUserLocation
      >
        {
          this.state.gyms.map((gym, index) => (
            this.getMarker(gym, `gym-${index}`, 'gym', 'blue')
          ))
        }
        {
          this.state.foods.map((food, index) => (
            this.getMarker(food, `food-${index}`, 'food', 'red')
          ))
        }
        {
          this.state.parks.map((park, index) => (
            this.getMarker(park, `park-${index}`, 'park', 'green')
          ))
        }
        {
          this.state.data.map((data) => (
            this.getUserMarker(data)
          ))
        }
      </MapView>
      <RkButton style={{ top: 15, left: 15 }} rkType='mapicon circle' onPress={() => this.onMenuPress('messages')}>
        <RkText rkType='awesome map'>{FontAwesome.message}</RkText>
      </RkButton>
      <RkButton style={{ top: 65, left: 15 }} rkType='mapicon circle' onPress={() => this.onMenuPress('notifications')}>
        <RkText rkType='awesome map'>{FontAwesome.notification}</RkText>
      </RkButton>
      <RkButton style={{ top: 115, left: 15 }} rkType='mapicon circle' onPress={() => this.onMenuPress('shop')}>
        <RkText rkType='awesome map'>{FontAwesome.shop}</RkText>
      </RkButton>
      <RkButton style={{ top: 15, right: 15 }} rkType='mapicon circle' onPress={() => this.onMenuPress('settings')}>
        <RkText rkType='awesome map'>{FontAwesome.settings}</RkText>
      </RkButton>
      <RkButton style={{ bottom: 15, left: 15 }} rkType='mapicon circle' onPress={() => this.onMenuPress('profile')}>
        <RkText rkType='awesome map'>{FontAwesome.profile}</RkText>
      </RkButton>
      <RkButton style={{ bottom: 15, right: 15 }} rkType='mapicon circle' onPress={() => this.onMenuPress('qrcode')}>
        <RkText rkType='awesome map'>{FontAwesome.qrcode}</RkText>
      </RkButton>
    </View>
  )
}

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
