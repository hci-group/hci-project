import React from 'react';
import { View, ScrollView, Text, Image } from 'react-native';
import { Rating } from 'react-native-elements';
import { RkStyleSheet, RkText, RkButton, RkTheme } from 'react-native-ui-kitten';
import { Avatar } from '../../components';
import { data } from '../../data';
import Parse from 'parse/react-native';
import photo from '../../data/img/Image1.png';
import { ProgressChart } from '../../components/';

export class PetProfile extends React.Component {
  static navigationOptions = {
    title: 'Pet Profile'.toUpperCase(),
  };

  state = {
    data: data.getUser(),
  };

  onSwapPressed = () => {
    const currentUser = Parse.User.current();
    const username = currentUser.get('username');
    this.props.navigation.navigate('UserProfile', { username, photo });
  }

  onActivityPressed = () => {
    this.props.navigation.navigate('Dashboard');
  }

  render = () => {
    const chartBackgroundStyle = { backgroundColor: RkTheme.current.colors.control.background };
    return (
      <ScrollView style={styles.root}>
        <View style={[styles.header, styles.bordered]}>
          <Image
            style={styles.image}
            source={require('../../data/img/pet1.png')}
          />
          {/* <Avatar img={this.state.data.petphoto} rkType='big' /> */}
          <RkText rkType='header2'
            style={{ color: 'white' }}>
            {`${this.state.data.petName}`}</RkText>
          {/* <GradientButton style={styles.button} text='FOLLOW' /> */}
          <View style={styles.userInfo}>
          <View style={styles.section}>
            <RkText></RkText>
            <RkButton
              style={{ backgroundColor: 'blue' }}
              contentStyle={{ color: 'white' }}
              onPress={this.onSwapPressed}
            >
              Swap Pet
          </RkButton>
          </View>
          <View style={styles.section}>
            <RkText></RkText>
            <RkButton
              style={{ backgroundColor: 'green' }}
              contentStyle={{ color: 'white' }}
              onPress={this.onActivityPressed}
            >
              Activities
          </RkButton>
          </View>
        </View>
      </View>
        <View>
          <View style={[styles.normal, styles.bordered]}>
            <RkText rkType='header2'>Story</RkText></View>
          <Text>
            {`
Wolfzara is a Dark Type Pet introduced in Generation III. Prior to Generation VI, it was a pure Psychic-type Pet.It evolves to Lazaro starting at level 30.

It is one of Wolfzara final forms, the other being Lazyas. It can Mega Evolve into Mega Lazyas when fused with another 5 star egg.
       
        `}
          </Text>

        </View>

        <View style={[styles.normal, styles.bordered]}>
          <RkText rkType='header2'>Attributes</RkText></View>
        <View style={styles.userInfo}>
          <View style={styles.section}>
            <RkText rkType='secondary1 hintColor'>Type</RkText>
            <Avatar img={this.state.data.petType} rkType='xlarge' />
          </View>

          <View style={styles.section}>
            <RkText rkType='secondary1 hintColor'>Grade</RkText>
            <Rating
              type="star"
              fractions={1}
              startingValue={5}
              imageSize={15}
            />
          </View>

          <View style={styles.section}>
            <RkText rkType='secondary1 hintColor'>Level</RkText>
            <RkText rkType='header3' style={styles.space}>{this.state.data.petlvl}</RkText>
          </View>

        </View>

        <View style={styles.userInfo}>
          <View style={styles.section}>
            <RkText rkType='secondary1 hintColor'>Weakness</RkText>
            <Avatar img={this.state.data.petWeak} rkType='xxlarge' />
          </View>

          <View style={styles.section}>
            <RkText rkType='secondary1 hintColor'>Health</RkText>
            <RkText rkType='header3' style={styles.space}>{this.state.data.petHp}</RkText>
          </View>

          <View style={styles.section}>
            <RkText rkType='secondary1 hintColor'>Attack</RkText>
            <RkText rkType='header3' style={styles.space}>{this.state.data.petattack}</RkText>
          </View>
        </View>

        <View style={[styles.chartBlock, chartBackgroundStyle, styles.normal, styles.bordered]}>
        <RkText rkType='header2'>Steps</RkText>
        </View>
        <View style={[styles.header1]}>
          <Image
            style={styles.image1}
            source={require('../../assets/images/party1.png')}
          />
        </View>
          <ProgressChart />
          <RkText/>
          <RkText/>
          <RkText/>
          <RkText/>
      </ScrollView>
    )
  };
}

const styles = RkStyleSheet.create(theme => ({
  root: {
    backgroundColor: theme.colors.screen.base,
  },
  chartBlock: {
    padding: 15,
    marginBottom: 15,
    justifyContent: 'center',
  },
  image: {
    flex: 1,
    width: 300,
    height: 300,
    resizeMode: 'contain'
  },
  image1: {
    flex: 1,
    width: 150,
    height: 150,
    resizeMode: 'contain'
  },
  header: {
    alignItems: 'center',
    paddingTop: 25,
    paddingBottom: 17,
    backgroundColor: '#663399',
  },

  header1: {
    alignItems: 'center',
    paddingTop: 25,
    paddingBottom: 17,
  },


  normal: {
    alignItems: 'center',

  },

  userInfo: {
    flexDirection: 'row',
    paddingVertical: 18,
  },
  bordered: {
    borderBottomWidth: 1,
    borderColor: theme.colors.border.base,
  },
  section: {
    flex: 1,
    alignItems: 'center',
  },
  space: {
    marginBottom: 3,
  },
  separator: {
    backgroundColor: theme.colors.border.base,
    alignSelf: 'center',
    flexDirection: 'row',
    flex: 0,
    width: 1,
    height: 42,
  },
  buttons: {
    alignItems: 'center',
  },
  button: {
    marginTop: 18,
    alignSelf: 'center',
    width: 140,

  },

  

}));
