import React from 'react';
import { Text, View, StyleSheet, Alert } from 'react-native';
import { Constants, Camera, Permissions, BarCodeScanner } from 'expo';

export class QRCode extends React.Component {
  static navigationOptions = {
    title: 'QR Code'.toUpperCase(),
  };

  state = {
    hasCameraPermission: null
  };

  componentDidMount() {
    this._requestCameraPermission();
  }

  _requestCameraPermission = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({
      hasCameraPermission: status === 'granted',
    });
  };

  _handleBarCodeRead = data => {
    Alert.alert('Scan successful!', 'Thank you for joining the raid!');
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.hasCameraPermission === null ?
          <Text>Requesting for camera permission</Text> :
          this.state.hasCameraPermission === false ?
            <Text>Camera permission is not granted</Text> :
            <Camera
              style={styles.camera}
              onBarCodeRead={this._handleBarCodeRead}
              barCodeScannerSettings={{
                barCodeTypes: [BarCodeScanner.Constants.BarCodeType.qr]
              }}
            />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  camera: {
    flex: 1,
    justifyContent: 'space-between',
  },
});