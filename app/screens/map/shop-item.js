import React from 'react';

import {
  ScrollView,
  Image,
  View,
  TouchableOpacity,
} from 'react-native';
import {
  RkCard,
  RkText,
  RkStyleSheet,
  RkButton,
} from 'react-native-ui-kitten';
import { data } from '../../data';
import {
  Avatar,
  SocialBar,
} from '../../components';
import NavigationType from '../../config/navigation/propTypes';

const moment = require('moment');

export class ShopItem extends React.Component {
  static propTypes = {
    navigation: NavigationType.isRequired,
  };
  static navigationOptions = {
    title: 'Unlock Skin'.toUpperCase(),
  };

  constructor(props) {
    super(props);
    const articleId = this.props.navigation.getParam('id', 1);
    this.data = data.getArticle(articleId);
  }

  onAvatarPressed = () => {
    this.props.navigation.navigate('UserProfile', { id: this.data.user.id });
  };

  render = () => (
    <ScrollView style={styles.root}>
      <RkCard rkType='article'>
        <Image rkCardImg source={this.data.photo} />
        <View rkCardHeader>
          <View>
            <RkText style={styles.title} rkType='header4'>{this.data.header}</RkText>
            <RkText rkType='secondary2 hintColor'>
            <RkText rkType='secondary2 hintColor'>Do not meet criteria yet</RkText>
            </RkText>
          </View>
          <RkButton
            style={{backgroundColor: 'red'}}
            contentStyle={{color: 'white'}}>Locked</RkButton>
        </View>
        <View rkCardContent>
          <View>
            <RkText rkType='primary3 bigLine'>{this.data.text}</RkText>
          </View>
        </View>
        <View rkCardFooter>
          
        </View>
      </RkCard>
    </ScrollView>
  )
}

const styles = RkStyleSheet.create(theme => ({
  root: {
    backgroundColor: theme.colors.screen.base,
  },
  title: {
    marginBottom: 5,
  },
}));
