import React from 'react';
import { FlatList, View, Image, Keyboard } from 'react-native';
import { RkButton, RkText, RkTextInput, RkAvoidKeyboard, RkStyleSheet, RkTheme } from 'react-native-ui-kitten';
import { FontAwesome } from '../../assets/icons';
import { Avatar } from '../../components/avatar';
import { scale } from '../../utils/scale';
import NavigationType from '../../config/navigation/propTypes';
import Parse from 'parse/react-native';
import Config from '../../config/config';

import photo1 from '../../data/img/Image1.png';

const moment = require('moment');

export class Chat extends React.Component {
  static propTypes = {
    navigation: NavigationType.isRequired,
  };

  static navigationOptions = ({ navigation }) => {
    let username = navigation.getParam('username', Parse.User.current().get('username'));
    let photo = navigation.getParam('photo', photo1);
    return ({
      headerTitle: Chat.renderNavigationTitle(navigation, username),
      headerRight: Chat.renderNavigationAvatar(navigation, photo),
    });
  };

  state = {
    data: [],
    username: ''
  }

  parseMessages = (data) => {
    return data.map((message) => ({
      name: message.name,
      type: message.name == this.state.username ? 'out' : 'in',
      time: moment(message.time),
      text: message.text
    }));
  }

  componentDidMount = () => {
    var currentUser = Parse.User.current();

    this.setState({ username: currentUser.get('username') });

    var client = new Parse.LiveQueryClient({
      applicationId: Config.appId,
      serverURL: Config.wsServerURL,
      javascriptKey: Config.javascriptKey,
      masterKey: Config.masterKey
    });
    client.open();

    const partyId = this.props.navigation.getParam('partyId', null)

    const Party = Parse.Object.extend("Party");
    const query = new Parse.Query(Party);
    query.equalTo("objectId", partyId);

    const subscription = client.subscribe(query);

    subscription.on('update', (object) => {
      this.getData(query)
    });

    this.getData(query)
  }

  getData = (query) => {
    query.first().then((party) => {
      const data = this.parseMessages(party.get('messages'));
      this.setState({ party, data }, () => {
        this.scrollToEnd();
      });
    });
  }

  extractItemKey = (item) => `${item.time}-${item.text}`;

  scrollToEnd = () => { this.refs.list.scrollToEnd(); };

  onInputChanged = (text) => { this.setState({ message: text }); };

  onSendButtonPressed = () => {
    const message = {
      name: this.state.username,
      time: moment().format(),
      text: this.state.message
    }

    const { party } = this.state;

    party.add("messages", message);
    party.save();

    this.setState({ message: '' });
    this.scrollToEnd();
  };


  static renderNavigationTitle = (navigation, username) => (
    <View style={styles.header}>
      <RkText rkType='header5'>{`${username}`}</RkText>
      <RkText rkType='secondary3 secondaryColor'>Online</RkText>
    </View>
  );

  static renderNavigationAvatar = (navigation, photo) => (
    <Avatar style={styles.avatar} rkType='small' img={photo} />
  );

  renderDate = (name, date) => (
    <RkText style={styles.time} rkType='secondary7 hintColor'>
      {name}
      {'\n'}
      {date.format('LT')}
    </RkText>
  );

  renderItem = ({ item }) => {
    const isIncoming = item.type === 'in';
    const backgroundColor = isIncoming
      ? RkTheme.current.colors.chat.messageInBackground
      : RkTheme.current.colors.chat.messageOutBackground;
    const itemStyle = isIncoming ? styles.itemIn : styles.itemOut;

    return (
      <View style={[styles.item, itemStyle]}>
        {!isIncoming && this.renderDate(item.name, item.time)}
        <View style={[styles.balloon, { backgroundColor }]}>
          <RkText rkType='primary2 mediumLine chat' style={{ color: 'white', paddingTop: 5 }}>{item.text}</RkText>
        </View>
        {isIncoming && this.renderDate(item.name, item.time)}
      </View>
    );
  };

  render = () => (
    <RkAvoidKeyboard
      style={styles.container}
      onResponderRelease={Keyboard.dismiss}>
      <FlatList
        ref='list'
        extraData={this.state}
        style={styles.list}
        data={this.state.data}
        keyExtractor={this.extractItemKey}
        renderItem={this.renderItem}
      />
      <View style={styles.footer}>
        <RkButton style={styles.plus} rkType='clear'>
          <RkText rkType='awesome secondaryColor'>{FontAwesome.plus}</RkText>
        </RkButton>
        <RkTextInput
          onFocus={this.scrollToEnd}
          onBlur={this.scrollToEnd}
          onChangeText={this.onInputChanged}
          value={this.state.message}
          rkType='row sticker'
          placeholder="Add a comment..."
        />
        <RkButton onPress={this.onSendButtonPressed} style={styles.send} rkType='circle highlight'>
          <Image source={require('../../assets/icons/sendIcon.png')} />
        </RkButton>
      </View>
    </RkAvoidKeyboard>

  )
}

const styles = RkStyleSheet.create(theme => ({
  header: {
    alignItems: 'center',
  },
  avatar: {
    marginRight: 16,
  },
  container: {
    flex: 1,
    backgroundColor: theme.colors.screen.base,
  },
  list: {
    paddingHorizontal: 17,
  },
  footer: {
    flexDirection: 'row',
    minHeight: 60,
    padding: 10,
    backgroundColor: theme.colors.screen.alter,
  },
  item: {
    marginVertical: 14,
    flex: 1,
    flexDirection: 'row',
  },
  itemIn: {},
  itemOut: {
    alignSelf: 'flex-end',
  },
  balloon: {
    maxWidth: scale(250),
    paddingHorizontal: 15,
    paddingTop: 10,
    paddingBottom: 15,
    borderRadius: 20,
  },
  time: {
    alignSelf: 'flex-end',
    margin: 15,
  },
  plus: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginRight: 7,
  },
  send: {
    width: 40,
    height: 40,
    marginLeft: 10,
  },
}));
