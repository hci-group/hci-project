import React from 'react';
import { View, Keyboard, Image } from 'react-native';
import { RkAvoidKeyboard, RkStyleSheet } from 'react-native-ui-kitten';
import { GradientButton } from '../../components/gradientButton';
import { scaleVertical } from '../../utils/scale';
import NavigationType from '../../config/navigation/propTypes';
import Parse from 'parse/react-native';

export class Marker extends React.Component {
  static propTypes = {
    navigation: NavigationType.isRequired,
  };
  static navigationOptions = {
    header: null,
  };

  state = {
    type: null,
    party: null
  }

  componentDidMount = () => {
    const currentUser = Parse.User.current();
    const currentUserName = currentUser.get('username')
    const placeName = this.props.navigation.getParam('place_name', null)

    const Party = Parse.Object.extend("Party");
    const query = new Parse.Query(Party);
    query.equalTo("place_name", placeName);

    query.first().then((party) => {
      if (party.get('owner_name') == currentUserName) {
        const partyId = party._getId();
        const members = party.get('members');
        this.props.navigation.replace('Owner', { partyId, members });
      }
      else if (party.get('members').includes(currentUserName)) {
        const partyId = party._getId();
        const members = party.get('members');
        this.props.navigation.replace('Member', { partyId, members });
      }
      else {
        this.setState({ type: 'join', party });
      }
    }).catch((error) => {
      this.setState({ type: 'create', party: null });
    });
  };

  onCreatePartyButtonPressed = () => {
    var currentUser = Parse.User.current();

    const Party = Parse.Object.extend("Party");
    const party = new Party();

    party.set("owner_id", currentUser._getId());
    party.set("owner_name", currentUser.get('username'));
    party.set("place_name", this.props.navigation.getParam('place_name', null));
    party.set("members", []);
    party.set("messages", []);

    party.save();

    const partyId = party._getId();
    const members = party.get('members');
    this.props.navigation.replace('Owner', { partyId, members });
  };

  onJoinPartyButtonPressed = () => {
    var currentUser = Parse.User.current();

    const { party } = this.state;

    party.add("members", currentUser.get('username'));
    party.save();

    const partyId = party._getId();
    const members = party.get('members');
    this.props.navigation.replace('Member', { partyId, members });
  }

  renderSelect = () => {
    if (this.state.type === 'create') { return this.renderCreate(); }
    else if (this.state.type === 'join') { return this.renderJoin(); }
  }

  renderCreate = () => (
    <View>
      <GradientButton
        style={styles.save}
        rkType='large'
        text='Create Party'
        onPress={this.onCreatePartyButtonPressed}
      />
    </View>
  )

  renderJoin = () => (
    <View>
      <GradientButton
        style={styles.save}
        rkType='large'
        text='Join Party'
        onPress={this.onJoinPartyButtonPressed}
        color='blue'
      />
    </View>
  )

  render = () => (
    <RkAvoidKeyboard
      style={styles.screen}
      onStartShouldSetResponder={() => true}
      onResponderRelease={() => Keyboard.dismiss()}>
      <View style={styles.container}>

      </View>
      <View style={styles.container}>

      </View>
      <View style={styles.container}>

      </View>
      <View style={styles.container}>

      </View>
      <View style={styles.container}>
        <Image
          source={require('../../assets/images/party.png')}
        />
      </View>
      <View style={styles.container}>

      </View>
      <View style={styles.container}>

      </View>
      <View style={styles.container}>

      </View>
      <View style={styles.container}>

      </View>
      <View style={styles.container}>

      </View>
      <View style={styles.container}>

      </View>
      <View style={styles.container}>

      </View>
      <View style={styles.container}>
        <Image
          source={require('../../assets/images/party2.png')}
        />
      </View>
      <View style={styles.content}>
        {
          this.renderSelect()
        }
      </View>
    </RkAvoidKeyboard>
  );
}

const styles = RkStyleSheet.create(theme => ({
  screen: {
    padding: scaleVertical(16),
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: theme.colors.screen.base,
  },
  image: {
    height: scaleVertical(77),
    resizeMode: 'contain',
  },
  header: {
    paddingBottom: scaleVertical(10),
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  content: {
    justifyContent: 'space-between',
  },
  save: {
    marginVertical: 20,
  },
  buttons: {
    flexDirection: 'row',
    marginBottom: scaleVertical(24),
    marginHorizontal: 24,
    justifyContent: 'space-around',
  },
  container: {
    paddingHorizontal: 17,
    paddingBottom: scaleVertical(22),
    alignItems: 'center',
    flex: -1,
  },
  textRow: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  button: {
    borderColor: theme.colors.border.solid,
  },
  section: {
    flex: 1,
    alignItems: 'center',
  },
  footer: {},
}));
