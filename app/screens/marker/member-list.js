import React from 'react';
import { FlatList, View, StyleSheet, TouchableOpacity } from 'react-native';
import { RkStyleSheet, RkText } from 'react-native-ui-kitten';
import { data as Data } from '../../data';
import { Avatar } from '../../components/avatar';
import NavigationType from '../../config/navigation/propTypes';

export class MemberList extends React.Component {
  static propTypes = {
    navigation: NavigationType.isRequired,
  };
  static navigationOptions = {
    title: 'Member List'.toUpperCase(),
  };

  state = {
    data: []
  };

  componentDidMount = () => {
    const members = this.props.navigation.getParam('members', [])
    const data = members.map((member, index) => ({
      id: index,
      username: member,
      photo: Data.getPhoto(index)
    }))
    this.setState({ data })
  }

  extractItemKey = (item) => `${item.id}`;

  onSearchInputChanged = (event) => { };

  onItemPressed = (item) => {
    this.props.navigation.navigate('UserProfile', { username: item.username, photo: item.photo });
  };

  renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => this.onItemPressed(item)}>
      <View style={styles.container}>
        <Avatar rkType='circle' style={styles.avatar} img={item.photo} />
        <RkText>{`${item.username}`}</RkText>
      </View>
    </TouchableOpacity>
  );

  renderSeparator = () => (
    <View style={styles.separator} />
  );

  render = () => (
    <FlatList
      style={styles.root}
      data={this.state.data}
      renderItem={this.renderItem}
      ItemSeparatorComponent={this.renderSeparator}
      keyExtractor={this.extractItemKey}
      enableEmptySections
    />
  )
}

const styles = RkStyleSheet.create(theme => ({
  root: {
    backgroundColor: theme.colors.screen.base,
  },
  searchContainer: {
    backgroundColor: theme.colors.screen.bold,
    paddingHorizontal: 16,
    paddingVertical: 10,
    height: 60,
    alignItems: 'center',
  },
  container: {
    flexDirection: 'row',
    padding: 16,
    alignItems: 'center',
  },
  avatar: {
    marginRight: 16,
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: theme.colors.border.base,
  },
}));
