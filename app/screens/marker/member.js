import React from 'react';
import { Rating } from 'react-native-elements';
import { View, ScrollView, Alert, Modal, Image } from 'react-native';
import { RkText, RkButton, RkStyleSheet } from 'react-native-ui-kitten';
import { Avatar } from '../../components/avatar';
import NavigationType from '../../config/navigation/propTypes';

import photo from '../../data/img/Image1.png';

import Parse from 'parse/react-native';

export class Member extends React.Component {
  static propTypes = {
    navigation: NavigationType.isRequired,
  };
  static navigationOptions = {
    title: 'Joined Party'.toUpperCase(),
  };

  state = {
    data: undefined,
    modalVisible: false,
  };

  componentDidMount = () => {
    const currentUser = Parse.User.current();
    const partyId = this.props.navigation.getParam('partyId', null)
    this.setState({
      partyId,
      username: currentUser.get('username')
    })
  }
  onMemberPress = () => {
    const members = this.props.navigation.getParam('members', null)
    this.props.navigation.navigate('MemberList', { members });
  };

  onGroupChat = () => {
    const partyId = this.props.navigation.getParam('partyId', null)
    this.props.navigation.navigate('Chat', { partyId });
  };

  onEndRaid = () => {
    this.setState({ modalVisible: true });
  };

  onEndRaid2 = () => {
    this.setState({ modalVisible: false });
    Alert.alert('Your rewards has been deposited.');
    this.props.navigation.goBack();
  };

  OnScanPress = () => {
    this.props.navigation.navigate('QRCode');
  };

  render = () => (
    <ScrollView style={styles.root}>
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => {
        }}>
        <View style={styles.section}>
          <RkText>Rewards</RkText>
          <RkText></RkText>
          <RkText></RkText>
          <RkText>EXP+500</RkText>
          <RkText></RkText>
          <RkText></RkText>
          <RkText>You got a 5 star egg!</RkText>
          <RkText></RkText>
          <RkText></RkText>
          <Rating
            type="star"
            fractions={1}
            startingValue={5}
            imageSize={15}
          />
          <Image
            source={require('../../assets/images/party.png')}
          />
          <RkText></RkText>
          <RkText></RkText>
          <RkButton
            style={{ backgroundColor: 'blue' }}
            contentStyle={{ color: 'white' }}
            onPress={() => this.onEndRaid2()}
          >Back</RkButton>
        </View>
      </Modal>
      <View style={[styles.header, styles.bordered]}>
        <Avatar img={photo} rkType='big' />
        <RkText rkType='header2'>{`${this.state.username}`}</RkText>
      </View>
      <View style={[styles.userInfo, styles.bordered]}>
        <View style={styles.section}>
          <RkText rkType='header4' style={marginRight = 5}>80</RkText>
          <RkText rkType='secondary1 hintColor'>Level</RkText>
        </View>
        <View style={styles.section}>
          <RkText></RkText>
          <RkButton
            style={{ backgroundColor: 'green' }}
            contentStyle={{ color: 'white' }}
            onPress={() => this.onMemberPress()}
          >Members</RkButton>
        </View>
        <View style={styles.section}>
          <RkText ></RkText>
          <Rating
            type="star"
            fractions={1}
            startingValue={3.6}
            imageSize={15}
          />
          <RkText></RkText>
          <RkText rkType='secondary1 hintColor'>Ratings</RkText>
        </View>
      </View>
      <View style={[styles.header, styles.bordered]}>
        <RkText rkType='secondary1 hintColor'>*Remember to scan party leader QRCode*</RkText>
        <RkText></RkText>
      </View>
      <View style={[styles.userInfo, styles.bordered]}>
        <View style={styles.section}>
          <RkButton
            style={{ backgroundColor: 'blue' }}
            contentStyle={{ color: 'white' }}
            onPress={() => this.OnScanPress()}
          >Scan Code</RkButton>
        </View>
        <View style={styles.section}>
          <RkButton
            style={{ backgroundColor: 'purple' }}
            contentStyle={{ color: 'white' }}
            onPress={() => this.onGroupChat()}
          >Chat</RkButton>
        </View>
        <View style={styles.section}>
          <RkButton
            style={{ backgroundColor: 'red' }}
            contentStyle={{ color: 'white' }}
            onPress={() => this.onEndRaid()}
          >Leave Raid</RkButton>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = RkStyleSheet.create(theme => ({
  root: {
    backgroundColor: theme.colors.screen.base,
  },
  header: {
    alignItems: 'center',
    paddingTop: 25,
    paddingBottom: 17,
  },
  userInfo: {
    flexDirection: 'row',
    paddingVertical: 18,
  },
  bordered: {
    borderBottomWidth: 1,
    borderColor: theme.colors.border.base,
  },
  section: {
    flex: 1,
    alignItems: 'center',
  },
  space: {
    marginBottom: 3,
  },

  separator: {
    backgroundColor: theme.colors.border.base,
    alignSelf: 'center',
    flexDirection: 'row',
    flex: 0,
    width: 1,
    height: 42,
  },
  buttons: {
    flexDirection: 'row',
    paddingVertical: 8,
  },
  button: {
    flex: 1,
    alignSelf: 'center',
  },
}));
