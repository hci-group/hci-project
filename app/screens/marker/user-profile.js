import React from 'react';
import { Rating } from 'react-native-elements';
import { View, ScrollView, Alert, Vibration } from 'react-native';
import { RkText, RkButton, RkStyleSheet } from 'react-native-ui-kitten';
import { Avatar } from '../../components/avatar';
import { Gallery } from '../../components/gallery';
import NavigationType from '../../config/navigation/propTypes';
import Parse from 'parse/react-native';

const pets = [
  require('../../data/img/pet1.png'),
  require('../../data/img/pet2.png'),
  require('../../data/img/pet3.png'),
  require('../../data/img/pet4.png'),
  require('../../data/img/pet5.png'),
];

export class UserProfile extends React.Component {
  static propTypes = {
    navigation: NavigationType.isRequired,
  };
  static navigationOptions = {
    title: 'User Profile'.toUpperCase(),
  };

  state = {
    data: undefined,
    count: 80,
    friend: 'Add Friend',
    username: '',
    photo: null
  };

  componentDidMount = () => {

    const username = this.props.navigation.getParam('username', null);
    const photo = this.props.navigation.getParam('photo', null);

    this.setState({ username, photo });
  }

  onItemPressed = () => {
    Alert.alert(
      'Friend added',
      'You are now friends ',
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false }
    )
    PATTERN = [500, 600];
    Vibration.vibrate(PATTERN, false);
    this.setState({ count: 81 });
    this.setState({ friend: 'Friends' });
  };

  onItemPressed1 = () => {
    //Insert the code to route to the chatlist here
  };

  render = () => (
    <ScrollView style={styles.root}>
      <View style={[styles.header, styles.bordered]}>
        <Avatar img={this.state.photo} rkType='big' />
        <RkText rkType='header2'>{`${this.state.username}`}</RkText>
      </View>
      <View style={[styles.userInfo, styles.bordered]}>
        <View style={styles.section}>
          <RkText rkType='header4' style={marginRight = 5}>80</RkText>
          <RkText rkType='secondary1 hintColor'>Level</RkText>
        </View>
        <View style={styles.section}>
          <RkText rkType='header4' style={marginRight = 5}>{`${this.state.count} `}</RkText>
          <RkText rkType='secondary1 hintColor'>Friends</RkText>
        </View>
        <View style={styles.section}>
          <RkText ></RkText>
          <Rating
            type="star"
            fractions={1}
            startingValue={3.6}
            imageSize={15}
          />
          <RkText></RkText>
          <RkText rkType='secondary1 hintColor'>Ratings</RkText>
        </View>
      </View>
      {
        this.state.username != Parse.User.current().get('username') ?
          <View style={[styles.buttons, styles.bordered]}>
            <RkButton style={styles.button} rkType='clear link' onPress={() => this.onItemPressed()}>{`${this.state.friend} `}</RkButton>
            <View style={styles.separator} />
            <RkButton style={styles.button} rkType='clear link' onPress={() => this.onItemPressed1()}>Message</RkButton>
          </View>
          : null
      }
      <View style={styles.bordered}>
        <View style={styles.section}>
        </View>
        <View style={styles.section}>
          <RkText ></RkText>
          <RkText ></RkText>
          <RkText rkType='secondary1 hintColor'>Pets Owned</RkText>
        </View>
        <View style={styles.section}>
          <RkText ></RkText>
          <RkText></RkText>
        </View>
      </View>
      <Gallery items={pets} />
    </ScrollView>
  );
}

const styles = RkStyleSheet.create(theme => ({
  root: {
    backgroundColor: theme.colors.screen.base,
  },
  header: {
    alignItems: 'center',
    paddingTop: 25,
    paddingBottom: 17,
  },
  userInfo: {
    flexDirection: 'row',
    paddingVertical: 18,
  },
  bordered: {
    borderBottomWidth: 1,
    borderColor: theme.colors.border.base,
  },
  section: {
    flex: 1,
    alignItems: 'center',
  },
  space: {
    marginBottom: 3,
  },

  separator: {
    backgroundColor: theme.colors.border.base,
    alignSelf: 'center',
    flexDirection: 'row',
    flex: 0,
    width: 1,
    height: 42,
  },
  buttons: {
    flexDirection: 'row',
    paddingVertical: 8,
  },
  button: {
    flex: 1,
    alignSelf: 'center',
  },
}));
